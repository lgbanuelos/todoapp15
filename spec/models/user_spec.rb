require 'spec_helper'

describe User do
  context 'Account creation' do
    it 'is valid if all user information is provided'
    it 'is invalid without a full name' do
      user = build(:user, :full_name => nil)
      user.should have(1).errors_on(:full_name)
    end
    it 'is invalid without a username' do
      user = build(:user, :username => nil)
      expect(user).to have(1).error_on(:username)
    end
    it 'is invalid if duplicated username (e.g., username is already used)'
    it 'is invalid if "password" and "password_confirmation" do not match'
  end
end
