
```
rake db:test:prepare
```


Add the following line to your `Gemfile`

```ruby
gem 'simplecov', :require => false, :group => :test
```

Add the following lines to `spec/spec_helper.rb` and `features/support/env.rb`

```ruby
require 'simplecov'
SimpleCov.start 'rails'
```


```ruby
require 'simplecov'
require 'simplecov-csv'
SimpleCov.formatter = SimpleCov::Formatter::CSVFormatter
SimpleCov.coverage_dir(ENV["COVERAGE_REPORTS"])
SimpleCov.start 'rails'
```


```ruby
group :test do
  gem 'simplecov'
  gem 'simplecov-csv'
  gem 'rspec_junit_formatter'

  gem 'capybara'
  gem 'selenium-webdriver'
end
```

`config/cucumber.yml`

```yaml
default: --profile ci --profile dev
ci: --format junit --out <%= ENV['CI_REPORTS'] %>
dev: <%= std_opts %> features
```

`shippable.yml`

```yaml
language: ruby

rvm:
  - 1.9.3

env:
  - CI_REPORTS=shippable/testresults COVERAGE_REPORTS=shippable/codecoverage

before_script:
  - mkdir -p shippable/testresults

script:
  - rspec -f JUnit -o shippable/testresults/results.xml

notifications:
 email:
     recipients:
         - exampleone@org.com
         - exampletwo@org.com
```

Alternatively, we can add the following lines to the `.rspec` file (however, this will affect the RSpec report format also for your development environment).

```
--format RspecJunitFormatter
--out <%= ENV['CI_REPORTS'] %>/results.xml
```



# Testing with actual browsers

```yaml
addons:
  firefox: "28.0"
services:
  - selenium

env:
  global:
    - DISPLAY=:99.0

before_script:
  - /etc/init.d/xvfb start

after_script:
  - /etc/init.d/xvfb stop
```
