class User < ActiveRecord::Base
  include ActiveModel::Validations
  attr_accessible :confirmed_at, :email, :full_name, :password, :password_confirmation, :role, :username

  validates_presence_of :full_name, :username
end
